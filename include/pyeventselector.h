#ifndef decisiontree_h
#define decisiontree_h 1

#include "TTree.h"
#include "TFile.h"
#include "TH1.h"
#include "TH1D.h"
#include "TCanvas.h"

#include "TLorentzVector.h"
#include "TLorentzRotation.h"
#include "TRotation.h"
#include "TVector3.h"
#include "TVector2.h"

#include "marlin/Processor.h"    
#include "lcio.h"
#include <string>


using namespace lcio ;
using namespace marlin ;


/**  Example processor for marlin.
 * 
 *  If compiled with MARLIN_USE_AIDA 
 *  it creates a histogram (cloud) of the MCParticle energies.
 * 
 *  <h4>Input - Prerequisites</h4>
 *  Needs the collection of MCParticles.
 *
 *  <h4>Output</h4> 
 *  A histogram.
 * 
 * @param CollectionName Name of the MCParticle collection
 * 
 * @author F. Gaede, DESY
 * @version $Id: MyProcessor.h,v 1.4 2005-10-11 12:57:39 gaede Exp $ 
 */

class decisiontree : public Processor {
  
 public:
  
  virtual Processor*  newProcessor() { return new decisiontree ; }
  
  
  decisiontree() ;
  
  /** Called at the begin of the job before anything is read.
   * Use to initialize the processor, e.g. book histograms.
   */
  virtual void init() ;
  
  /** Called for every run.
   */
  virtual void processRunHeader( LCRunHeader* run ) ;
  
  /** Called for every event - the working horse.
   */
  virtual void processEvent( LCEvent * evt ) ; 
  
  
  virtual void check( LCEvent * evt ) ; 
  
  
  /** Called after data processing for clean up.
   */
  virtual void end() ;
  
  
 protected:

  /** Input collection name.
   */
  std::string _colName ;
  std::string _mccolName ;
  std::string _pandacolName;
  std::string _coljetName;
  
  bool fullJetevent;
  bool semiLeptonevent;
  bool tauevent; 
  int fjcounter;
  int slcounter;
  int taucounter;
  
  bool nolevent;
  int nlcounter;
  
  int isolated;
  
  

  

  int _nRun ;
  int _nEvt ;
  
  TFile* file;
  TTree* tree;
  TH1D* h_bbbtag;
  TH1D* h_cbbtag;
  TH1D* h_bcctag;
  TH1D* h_ccctag;
  TH1D* h_budstag;
  TH1D* h_cudstag;
    


} ;

#endif



